INTRODUCTION
------------

D3 Taxonomy Mindmap plugin shows the Drupal Category or any taxonomy in an interactive tree layout. It can be used to generate mind maps using the Drupal taxonomy.This module adds a block that can be published in the desired region to show the treeview of the taxonomy terms belonging to the vocabulary selected in the block settings

  * For a demo of the plugin, visit the project page: http://www.anshisolutions.com/content/d3-taxonomy-mindmap
  * To submit bug reports and feature suggestions, or track changes send a mail to ceo@anshisolutions.com

REQUIREMENTS
------------
     
No special requirements

INSTALLATION
-------------
INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
----------

The module has no menu or modifiable settings. There is no configuration.

TROUBLESHOOTING
---------------------



 FAQ
 ---

     Q: How can I show the tree view with a given taxonomy?
     A: This module creates a drupal block where you can select the vocabulary containing the taxonomy terms to be shown in the treeview .


MAINTAINERS
-----------

Current maintainers: 
  * anshisolutions - https://www.drupal.org/u/anshisolutions
  * Neelima Rajesh - https://www.drupal.org/u/neelima-rajesh

This project has been sponsored by:

    * ANSHI SOLUTIONS
       Specialized in web designing and development by offering various services in Drupal, Joomla and Wordpress. Visit https://www.anshisolutions.com for more information.




